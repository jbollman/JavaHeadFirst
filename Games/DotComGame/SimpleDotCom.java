class SimpleDotCom {
     int[] locationCells;
     int numOfHits = 0;

    public String checkYourself(String userGuess) {
        int guess = Integer.parseInt(userGuess);
        //return either 'hit', 'miss' or 'kill';
        String result = "miss";
        
        for (int cell : locationCells) {
            if (guess == cell) {
                result =  "hit";
                numOfHits++;
                break;
            }else{
                result =  "miss";
                break;
            }
        }
        if (numOfHits == locationCells.length){
            result = "kill";
        }
        System.out.println(result);
        return result;
        
        
        
    }
    public void setLocationCells(int[] locs){
        locationCells = locs;
    }
}
